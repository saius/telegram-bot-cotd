from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    CommandHandler,
    CallbackQueryHandler,
    ConversationHandler,
    MessageHandler,
    Filters,
)

from bot.constants import CONVERSATIONS
from bot.db import Vars
from bot.data_store import MensajesPendientes
from bot.utils import escape_markdown_entities

# constantes de states
SEND_CAUSE, CAUSE_MSG = CONVERSATIONS.create_consts(2)

# constantes de botones (namespace casero con "<comando>_<n>")
YES, NO, CANCEL = [f'{CONVERSATIONS.ADMIN_REJECT}_{i}' for i in range(3)]

def create_handler():
    return ConversationHandler(
        entry_points=[CallbackQueryHandler(entry_command, pattern='^' + str(CONVERSATIONS.ADMIN_REJECT) + '-.*')],
        states={
            SEND_CAUSE: [
                CallbackQueryHandler(send_cause_yes, pattern='^' + YES + '$'),
                CallbackQueryHandler(send_cause_no, pattern='^' + NO + '$'),
                CallbackQueryHandler(send_cause_cancel, pattern='^' + CANCEL + '$'),
            ],
            CAUSE_MSG: [MessageHandler((Filters.text & ~Filters.command), input_cause_msg)],
        },
        fallbacks=[
            CommandHandler('return', return_command),
            # esto es para evitar múltiples rechazos al mismo tiempo
            CallbackQueryHandler(entry_command_block, pattern='^' + str(CONVERSATIONS.ADMIN_REJECT) + '-.*')
        ],
    )

def get_username_from_querycb(update):
    query = update.callback_query
    return query.data.split('-')[1]

def entry_command_block(update, context):
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer(
        text='Ya hay otra operación de rechazo en transcurso. Terminala o cancelala para comenzar otra.',
        show_alert=True)

    # no cambiar el state actual (de la otra operación)
    return None

def entry_command(update, context):
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer()

    username = get_username_from_querycb(update)
    context.chat_data['rejecting_username'] = username

    # InlineKeyboardButton -  https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
    keyboard = [
        [
            InlineKeyboardButton("Sí", callback_data=YES),
            InlineKeyboardButton("No", callback_data=NO),
        ],
        [InlineKeyboardButton("<< Volver", callback_data=CANCEL)],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    reply_text = '⏺ ¿Querés mandarle una explicación particular?'

    query.edit_message_text(reply_text, reply_markup=reply_markup)

    return SEND_CAUSE

def render_main_menu(update, context):
    username = context.chat_data['rejecting_username']

    mensaje_pendiente = MensajesPendientes.get_user(context, username)

    # NOTA: este mismo código se usa en start.py función post_yes !
    msg = f'Lx usuarix @{username} quiere postear el siguiente mensaje:\n{mensaje_pendiente.msg}'

    # InlineKeyboardButton -  https://python-telegram-bot.readthedocs.io/en/stable/telegram.inlinekeyboardbutton.html
    keyboard = [
        [
            InlineKeyboardButton("Admitir", callback_data=f'{CONVERSATIONS.ADMIN_ADMIT}-{username}'),
            InlineKeyboardButton("Rechazar", callback_data=f'{CONVERSATIONS.ADMIN_REJECT}-{username}'),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    if query:
        query.answer()
        query.edit_message_text(
            escape_markdown_entities(msg),
            parse_mode='MarkdownV2',
            reply_markup=reply_markup)
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=escape_markdown_entities(msg),
            parse_mode='MarkdownV2',
            reply_markup=reply_markup)

    del context.chat_data['rejecting_username']

    return ConversationHandler.END

def send_cause_cancel(update, context):
    return render_main_menu(update, context)

def send_cause_no(update, context):
    return reject_msg(update, context)

def send_cause_yes(update, context):
    username = context.chat_data['rejecting_username']

    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    query.answer()
    query.edit_message_text('⏺ Okey, mandame el mensaje que querés que reciba (o volvé con /return):')

    return CAUSE_MSG

def return_command(update, context):
    return render_main_menu(update, context)

def input_cause_msg(update, context):
    # https://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html#telegram.Message.text_markdown_v2
    # si usamos text_markdown_v2 sin _urled no se arman previews de los links
    text = update.message.text_markdown_v2
    return reject_msg(update, context, text)

def reject_msg(update, context, cause=None):
    username = context.chat_data['rejecting_username']

    # traemos data de la bot_data
    mensaje_pendiente = MensajesPendientes.get_user(context, username)

    if not cause:
        text = '✴️ El mensaje que quisiste enviar al canal @CotDRLab fue rechazado :/'
    else:
        text = f'✴️ El mensaje que quisiste enviar al canal @CotDRLab fue' \
         f' rechazado por el siguiente motivo:\n{cause}'

    # le avisamos al usuarie
    context.bot.send_message(
        chat_id=mensaje_pendiente.chat_id,
        text=text)

    reply_msg = '✅ Usuarix notificadx'
    # CallbackQuery - https://python-telegram-bot.readthedocs.io/en/stable/telegram.callbackquery.html
    query = update.callback_query
    if query:
        query.answer()
        query.edit_message_text(reply_msg)
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text=reply_msg)

    # borramos de la bot_data
    MensajesPendientes.remove_user(context, username)

    del context.chat_data['rejecting_username']

    return ConversationHandler.END
