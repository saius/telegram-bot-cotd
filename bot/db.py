import datetime

from peewee import *

from bot.env import try_get_multiple, try_get

DB_FILE, LOGGING_SQL = try_get_multiple('DB_FILE, LOGGING_SQL')
LOGGING_SQL = LOGGING_SQL.lower() in ['1', 'true', 'yes', 'y']

# pragmas - https://docs.peewee-orm.com/en/latest/peewee/database.html#recommended-settings
db = SqliteDatabase(DB_FILE, pragmas={'journal_mode': 'wal'})

if LOGGING_SQL:
    import logging
    logger = logging.getLogger('peewee')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)

# Models and Fields - https://docs.peewee-orm.com/en/latest/peewee/models.html
class BaseModel(Model):
    class Meta:
        database = db

class Admin(BaseModel):
    username = CharField(unique=True)

    def has_user(username):
        try:
            return Admin.get(Admin.username == username)
        except DoesNotExist:
            return None

# TODO
class Whitelist(BaseModel):
    username = CharField(unique=True)

class Vars(BaseModel):
    key = CharField(unique=True)
    value = TextField(default=None)

    def get_key(key):
        try:
            return Vars.get(Vars.key == key)
        except DoesNotExist:
            return None
    def set_key(key, value):
        key_obj = Vars.get_key(key)
        if not key_obj:
            key_obj = Vars.create(key=key, value=value)
        else:
            key_obj.value = value
            key_obj.save()
        return key_obj

    def get_admin_group():
        key_obj = Vars.get_key('admin_group')
        return key_obj.value if key_obj else None
    def set_admin_group(value):
        return Vars.set_key('admin_group', value)

    def get_post_channel():
        key_obj = Vars.get_key('post_channel')
        return key_obj.value if key_obj else None
    def set_post_channel(value):
        if value[0] != '@':
            value = f'@{value}'
        return Vars.set_key('post_channel', value)

DB_MODELS = [Admin, Whitelist, Vars]

def recreate(force=False):
    if not force and input('Estás segurx que querés re/crear la DB? [y/n] ').lower() != 'y':
        print('Cancelado')
        return

    INITIAL_ADMINS=try_get('INITIAL_ADMINS')

    if db.is_closed():
        db.connect()


    print('Recreando DB')
    db.drop_tables(DB_MODELS)
    db.create_tables(DB_MODELS)

    print('Insertando admins por defecto')
    admins = INITIAL_ADMINS.split(',')
    for admin in admins:
        Admin.create(username=admin.strip())

    db.close()
    print('DB recreada')
